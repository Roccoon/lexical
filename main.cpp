#include <iostream>
#include <cstdio>
#include "Scanner.h"
#define DEBUG
using namespace std;

int main()
{
	Scanner scan = Scanner("program_test.pas", "Grammar.txt");
	string token;
	while (!scan.isEnd())
	{
		token = scan.getToken();
		if (!scan.isEnd())
			cout << scan.getFileName() << ":" << scan.getLine() << ":" << scan.getColumn() << "\t" << scan.getTokenType() << "\t" << token << endl;
	}

	system("pause");
}