#include "DFA.h"
#include <iostream>
#include <map>
#include <queue>
#define DEBUG
using namespace std;

/*
* 1. 读入 A->aB 或 A->a
* 2. 若A不在map中，把A加入到map中；若A在map中，则进入第三步
* 3. 若B不存在，则把(a, B)加入到A的multimap中，然后把B加入到map中；若B存在，则把(a, y)加入到A的multimap中
* 4. 重复第1~3步，直到把文法全部读入
*/

DFA::DFA()
{
}

DFA::DFA(vector<tuple<string, string, string>> grammarSource)
	:grammar(grammarSource)
{
}

void DFA::makeNFA()
{
	string A, a, B;
	int len = grammar.size();
	tuple<string, string, string> temp = grammar[0];
	this->begin = std::get<0>(temp);
	for(int i = 1; i<len; i++)
	{
		A = std::get<0>(grammar[i]);
		a = std::get<1>(grammar[i]);
		B = std::get<2>(grammar[i]);
		//统计终结符Vn和非终结符Vt
		vn.insert(a);
		vt.insert(A);
		if (B != "###")
			vt.insert(B);

		map<string, multimap<string, string> >::iterator res;
		res = nfa.find(A);
		if (res == nfa.end())
		{
			nfa.insert(pair<string, multimap<string, string> >(A, multimap<string, string>()));
			res = nfa.find(A);
		}
		res->second.insert(pair<string, string>(a, B));
	}

#ifdef DEBUG
	map<string, multimap<string, string> >::iterator i;
	multimap<string, string>::iterator j;
	for (i = nfa.begin(); i != nfa.end(); i++)
	{
		multimap<string, string> t = i->second;
		for (j = t.begin(); j != t.end(); j++)
		{
			cout << i->first << " --- " << j->first << " --> " << j->second << endl;
		}
	}
	cout << "END" << endl;
#endif // DEBUG

}

vector< string> DFA::split(string str, string pattern)
{
	vector<string> ret;
	if (pattern.empty()) return ret;
	size_t start = 0, index = str.find_first_of(pattern, 0);
	while (index != str.npos)
	{
		if (start != index)
			ret.push_back(str.substr(start, index - start));
		start = index + 1;
		index = str.find_first_of(pattern, start);
	}
	if (!str.substr(start).empty())
		ret.push_back(str.substr(start));
	return ret;
}

/*
 *
 */

void DFA::NfaToDfa()
{
	int id = 0;
	set<string> s;
	s.insert(begin);
	queue<pair<int, set<string> > > c;
	c.push(pair<int, set<string> >(id++, s));
	set<int> visit;
	set<set<string> > buffer;
	hash.insert(pair<set<string>, int>(s, 0));
	rhash.insert(pair<int, set<string> >(0, s));
	
	while (!c.empty())
	{
		pair<int, set<string> > t = c.front();
		c.pop();
		buffer.insert(t.second);
		auto pos = visit.find(t.first);
		if (pos != visit.end())
			continue;
		visit.insert(t.first);
		
		map<string, int> temp;
		set<string>::iterator i;
		for (i = vn.begin(); i != vn.end(); i++)
		{
			set<string> u = move(t.second, *i);
			auto pos = buffer.find(u);
			if (!(findInQueue(c, u) || pos != buffer.end()) && !u.empty())
			{
				hash.insert(pair<set<string>, int>(u, id));
				rhash.insert(pair<int, set<string> >(id, u));
				c.push(pair<int, set<string> >(id++, u));
			}
			if (!u.empty())
			{
				map<set<string>, int>::iterator res = hash.find(u);
				temp.insert(pair<string, int>(*i, res->second));
			}
		}

		dfa.insert(pair<int, map<string, int> >(t.first, temp));
	}

#ifdef DEBUG
	map<int, map<string, int> >::iterator i;
	map<string, int>::iterator j;
	for (i = dfa.begin(); i != dfa.end(); i++)
	{
		map<string, int> t = i->second;
		for (j = t.begin(); j != t.end(); j++)
		{
			cout << i->first << " --- " << j->first << " --> " << j->second << endl;
		}
	}

	map<int, set<string> >::iterator k;
	set<string>::iterator p;
	for (k = rhash.begin(); k != rhash.end(); k++)
	{
		cout << k->first << endl;
		for (p = k->second.begin(); p != k->second.end(); p++)
		{
			cout << "\t" << *p << endl;
		}
	}
	cout << "END" << endl;
#endif // DEBUG
}

pair<int, map<string, int>> DFA::find(int s)
{
	map<int, map<string, int> >::iterator res;
	res = dfa.find(s);
	if(res != dfa.end())
		return pair<int, map<string, int>>(res->first, res->second);
	return pair<int, map<string, int>>();
}

string DFA::getType(int state)
{
	map<int, set<string> >::iterator k = rhash.find(state);
	set<string> temp = k->second;
	string res = *(temp.begin());
	std::transform(res.begin(), res.end(), res.begin(), ::toupper);
	return res;
}

/*
 * 转移函数
 */

bool DFA::findInQueue(queue<pair<int, set<string> > > s, set<string> t)
{
	while (!s.empty())
	{
		set<string> u = s.front().second;
		s.pop();
		if (u == t)
			return true;
	}
	return false;
}

set<string> DFA::move(const set<string> s, string edge)
{
	set<string>::iterator i;
	set<string> ans;
	map<string, multimap<string, string> >::iterator res;
	multimap<string, string>::iterator d;
	for (i = s.begin(); i != s.end(); i++)
	{
		res = nfa.find(*i);
		if (res == nfa.end())
			continue;
		d = res->second.find(edge);
		int count = res->second.count(edge);
		for (int k = 0; k < count; k++, d++)
		{
			ans.insert(d->second);
		}

	}
	return ans;
}