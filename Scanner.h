#pragma once
#include <iostream>
#include <fstream>
#include "DFA.h"
class Scanner
{
public:
	Scanner(std::string srcFileName, std::string grammar);
	string getToken();
	string getFileName();
	string getTokenType();
	bool isEnd();
	long getLine();
	long getColumn();
private:
	void getNextChar();
	char peekChar();
	void preprocess();
	vector<tuple<string, string, string> > getGrammar(string grammerFile);
	char transform(char ch);
public:
	//enum class State
	//{
	//	NONE,
	//	UNKNOW,
	//	END_OF_FILE,
	//	ACCEPT
	//};
private:
	std::string fileName;
	std::string grammarFile;
	std::ifstream input;
	long line;
	long bLine;
	long column;
	long bColumn;
	char currentChar;
	std::string buffer;
	int state;
	bool errorFlag;
	int nearestAccept;
	int nearestState;
	string nextRead;
	DFA dfa;
	set<string> limiters;
	set<string> keywords;
	set<string> operat;
};

