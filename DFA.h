#pragma once
#include <map>
#include <set>
#include <queue>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;
class DFA
{
public:
	DFA();
	DFA(vector<tuple<string, string, string> > grammarSource);
	void makeNFA();
	void NfaToDfa();
	pair<int, map<string, int> > find(int s);
	string getType(int state);
private:
	string begin;
	set<string> vn;
	set<string> vt;
	map<string, multimap<string, string> > nfa;
	map<int, map<string, int> > dfa;
	map<set<string>, int> hash;
	map<int, set<string> > rhash;
private:
	bool findInQueue(queue<pair<int, set<string> > > s, set<string> t);
	set<string> move(const set<string> s, string edge);
	vector< string> split(string str, string pattern);
	vector<tuple<string, string, string> > grammar;
};